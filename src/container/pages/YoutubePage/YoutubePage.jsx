import React, { Component, Fragment } from "react";
import YoutubeComp from "../../../component/YoutubeComp/YoutubeComp";

class YoutubePage extends Component {
    render() {
        return (
            <Fragment>
                <p>Halaman YouTube</p>
                <hr />
                <YoutubeComp
                    time="07.12"
                    title="Tutorial React JS - Bagian 1"
                    desc="2x ditonton. 2 hari yang lalu" />
                <YoutubeComp
                    time="11.03"
                    title="Tutorial React JS - Bagian 2"
                    desc="200x ditonton. 10 hari yang lalu" />
                <YoutubeComp
                    time="14.11"
                    title="Tutorial React JS - Bagian 3"
                    desc="500x ditonton. 4 hari yang lalu" />
                <YoutubeComp
                    time="20.01"
                    title="Tutorial React JS - Bagian 4"
                    desc="1k ditonton. 14 hari yang lalu" />
                <YoutubeComp />
            </Fragment >
        );
    }
}
export default YoutubePage;