import React from "react";
import { Component, Fragment } from "react/cjs/react.production.min";
import CardProduct from "./CardProduct/CardProduct";
import './Product.css'

class Product extends Component {
    state = {
        order: 4,
        name: 'Veny'
    }

    handleCounterChange = (newValue) => {
        this.setState({
            order: newValue
        })
    }

    render() {
        return (
            <Fragment>
                <p>Halaman Product</p>
                <hr/>
                <div className="header">
                    <div className="logo">
                        <img src="https://res.cloudinary.com/crunchbase-production/image/upload/c_lpad,f_auto,q_auto:eco,dpr_1/jznhmlcmc5dia2qrmc7r" alt="product"/>
                    </div>
                    <div className="troley">
                        <img src="https://static.thenounproject.com/png/1138102-200.png"alt="product"/>
                        <div className="count">{this.state.order}</div>
                    </div>
                </div>
                <CardProduct onCounterChange={(value) => this.handleCounterChange(value)} />
            </Fragment>
        )
    }
}
export default Product;