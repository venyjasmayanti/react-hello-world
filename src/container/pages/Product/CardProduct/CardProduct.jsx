import { Component } from 'react';

class CardProduct extends Component {
    state = {
        order: 4
    }

    handleCounterChange = (newvalue) => {
        this.props.onCounterChange(newvalue);
        }
        

    handlePlus = () => {
        this.setState({
            order: this.state.order + 1
        }, () => {
            this.handleCounterChange(this.state.order);
        })
    }
    handleMinus = () => {
        if (this.state.order > 0) {
            this.setState({
                order: this.state.order - 1
            }, () => {
                this.handleCounterChange(this.state.order);
            })
        }
    }

    render() {
        return (
            <div className="card">
                <div className="img-thumb-prod">
                    <img src="https://asset-a.grid.id/crop/0x0:0x0/700x465/photo/2020/07/02/97545327.jpg" alt="Product_images"/>
                </div>
                <p className="product-title">Daging Ayam Beku (Frozen Food) - Paha Ayam Netto = 1Kg</p>
                <p className="product-price">Rp 410.000</p>
                <div className="counter">
                    <button className="minus" onClick={this.handleMinus}>-</button>
                    <input className= "counttext" type="text" value={this.state.order} />
                    <button className="plus" onClick={this.handlePlus}>+</button>

                </div>
            </div>
        )
    }
}
export default CardProduct;