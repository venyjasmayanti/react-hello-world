import React, { Component, Fragment } from 'react';
import './LifeCycleComp.css'

class LifeCycleComp extends Component {
    constructor(props) {
        super(props);
        this.state = {
            count: 1
        }
        console.log('constructor')
    }

    static getDrivedStateFromProps(props, state) {
        console.log('getDrivedStateFromProps')
        return null;

    }
    componentDidMount() {
        console.log('componentDidMount')
    }

    shouldComponentUpdate(nextProps, nextState) {
        console.group('shouldComponentUpdate')
        console.log('nextProps:', nextProps);
        console.log('nextState:', nextState);
        console.groupEnd();
        return true;

    }
    getSnapshotBeforeUpdate(prevProps, prevState) {
        console.log('getSnapshotBeforeUpdate')
        return null;

    }
    componentDidUpdate(prevProps, prevState, snapshot) {
        console.log('componentDidUpdate')

    }
    componentWillUnmount() {
        console.log('componentWillUnmount')

    }

    changeCount = () => {
        this.setState({
            count: this.state.count + 1
        })
    }

    render() {
        return (
            <Fragment>
                <p>Halaman LifeCycle</p>
                <hr />
                <button className="btn" onClick={this.changeCount}>Component Button {this.state.count}</button>
            </Fragment>
        )
    }
}
export default LifeCycleComp;