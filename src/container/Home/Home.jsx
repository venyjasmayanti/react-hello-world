//library
import React, { } from "react";
import { BrowserRouter as Router, Routes, Route, Link } from 'react-router-dom';

//pages
import BlogPost from "../pages/BlogPost/BlogPost";
import DetailPost from "../pages/BlogPost/DetailPost/DetailPost";
import LifeCycleComp from "../pages/LifeCycleComp/LifeCycleComp";
import Product from "../pages/Product/Product";
import YoutubePage from "../pages/YoutubePage/YoutubePage";

//style
import './Home.css';


function Home() {
    return (
        <Router>
            <div className="navigation">
                <Link to="/" >Blog Post</Link>
                <Link to="/product" >Product</Link>
                <Link to="/lifecycle" >LifeCycle</Link>
                <Link to="/youtube" >YouTube</Link>
            </div>
            <Routes>
                <Route path="/" element={<BlogPost />} />
                <Route path="/detail-post/:id" element={<DetailPost />} />
                <Route path="/product" element={<Product />} />
                <Route path="/lifecycle" element={<LifeCycleComp />} />
                <Route path="/youtube" element={<YoutubePage />} />
            </Routes>
        </Router>
    );
}

export default Home;