import React, { Fragment } from "react";
import './YoutubeComp.css';

const YoutubeComp = (props) => {
    return (
        <Fragment>
            <div className="youtube-wrapper">
                <div className="img-thumb">
                    <img src="https://kopi.dev/static/29c7e361449d99b9f6849599d02b24ea/1*N9r_AVMQm7HIZt3X6z7FuA.png" alt="youtubeimages" />
                    <p className="time">{props.time}</p>
                </div>
                <p className="title">{props.title}</p>
                <p className="desc">{props.desc}</p>
            </div>
        </Fragment>
    )
}

YoutubeComp.defaultProps = {
    time: '00.00',
    title: 'Title here',
    desc: 'xx ditonton. x hari yang lalu'
}

export default YoutubeComp;